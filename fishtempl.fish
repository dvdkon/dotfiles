# This file provides a simple templating system based on interleaved fish
# meant primarily for config files.
# Lines prefixed with "$" are treated as fish code, while other lines
# are echoed directly
# Inside those lines, you can use "${ expr }" to evaluate "expr" in fish

function compile_file
	while read line
		if echo $line | grep -q '^[ '\t']*\$'
			echo $line | sed -e 's/^[ '\t']*\$[ ]*\(.*\)/\1/'
		else
			set line_escaped ( \
				echo $line | sed -e "s/\\\\/\\\\\\\\/g; s/'/\\\\'/g")
			# TODO: Better interpolation, this will fail with something like
			# "${ "}" }". But doing this correctly is probably out-of-scope
			set line_interpolated ( \
				echo $line_escaped | sed -e 's/\${\([^}]*\)}/\'\1\'/g')
			echo "echo '$line_interpolated'"
		end
	end < $argv[1]
end

function template_file
	set IFS_old $IFS
	set IFS ""
	eval 'set IFS \n'\n(compile_file $argv[1])
	set IFS $IFS_old
end
