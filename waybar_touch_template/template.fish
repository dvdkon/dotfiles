#!/usr/bin/env fish
cd (dirname (status --current-filename))
source ../fishtempl.fish

for dir in vert horiz
	set DIR $dir
	template_file config.ftp > ../waybar_touch/.config/waybar-$DIR/config
	template_file style.css.ftp > ../waybar_touch/.config/waybar-$DIR/style.css
	template_file waybar.service.ftp > ../waybar_touch/.config/systemd/user/waybar-$DIR.service
end
