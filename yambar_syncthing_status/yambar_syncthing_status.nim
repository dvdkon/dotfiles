import httpclient, os, xmlparser, xmltree, strformat, strutils, json, uri

let folder_id = param_str(1)

let check_interval_ms = 10000

let config_location = get_env("HOME") & "/.config/syncthing/config.xml"
let config_xml = load_xml(config_location)
let config_gui = config_xml.child("gui")
let api_host = config_gui.child("address").inner_text
let api_key = config_gui.child("apikey").inner_text

let api_base_url = parse_uri(fmt"http://{api_host}/rest")
let auth_headers = new_http_headers({"X-API-Key": api_key})
let global_http_client = new_http_client(headers = auth_headers)

# Wait for Syncthing to start
while true:
    try:
        discard global_http_client.get(api_base_url / "system" / "version")
        break
    except OSError:
        stderr.write_line("Connection failed, waiting for availability")
        sleep(1000)
stderr.write_line("Syncthing available, starting!")

# Global state
var last_event_id = 0
var paused = true
var idle = true
var state_str = "Starting"

proc print_state() =
    echo("paused|bool|", paused)
    echo("idle|bool|", idle)
    echo("state|string|", state_str)
    echo()

# Print initial state to make Yambar happy
print_state()

# Initial state check
let folder_config =
    global_http_client.get(api_base_url / "config" / "folders" / folder_id)
    .body.parse_json()
paused = folder_config["paused"].get_bool()

while true:
    let events =
        global_http_client.get(
            api_base_url / "events" ? {
                "since": $last_event_id,
                "events": "FolderPaused,FolderResumed,FolderSummary",
            })
        .body
        .parse_json()

    for event in events:
        last_event_id = event["id"].get_int()
        let typ = event["type"].get_str()
        case typ:
            of "FolderPaused":
                if event["data"]["id"].get_str() != folder_id: continue
                paused = true
            of "FolderResumed":
                if event["data"]["id"].get_str() != folder_id: continue
                paused = false
            of "FolderSummary":
                if event["data"]["folder"].get_str() != folder_id: continue
                if event["data"]["summary"]["watchError"].get_str() != "":
                    state_str = "Error!"
                    continue
                let state = event["data"]["summary"]["state"].get_str()
                case state:
                    of "idle":
                        idle = true
                    of "syncing":
                        idle = false
                        state_str = "Syncing"
                    of "scanning":
                        idle = false
                        state_str = "Scanning"
                    else:
                        idle = false
                        state_str = fmt"Unknown: {state}"
            else:
                stderr.write_line("Unknown event! ", typ)

    print_state()

    sleep(check_interval_ms)
