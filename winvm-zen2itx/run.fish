#!/usr/bin/env fish

#set SPICE_SOCKET (mktemp -u /tmp/spice.XXX)
set SPICE_SOCKET /tmp/winvm.spice

set qemu_args
function qa; set -a qemu_args $argv; end

if [ (count $argv) -lt 3 ]
	echo "Usage: <memory size (GB)> <core count> <passthrough GPU> <qemu args>" >&2
	exit 1
end

set mem_size $argv[1]
set core_count $argv[2]
set gpu $argv[3]

qa -enable-kvm
# Slightly newer chipset, might be beneficial, surely won't hurt
qa -machine q35
# See https://blog.wikichoon.com/2014/07/enabling-hyper-v-enlightenments-with-kvm.html
#qa -cpu host,hv_relaxed,hv_spinlocks=0x1fff,hv_vapic,hv_time,topoext
#qa -cpu core2duo,nx
# A QEMU upgrade (or something else) broke host,topoext, so use EPYC for now
qa -cpu EPYC,hv_passthrough # hv_passthrough should enable all supported enlightenments
qa -smp cores=$core_count,threads=2
qa -m {$mem_size}G

qa -drive if=pflash,format=raw,readonly=on,file=/usr/share/edk2-ovmf/x64/OVMF_CODE.fd
qa -drive if=pflash,format=raw,file=uefi_vars.fd

qa -device qemu-xhci,id=xhci

if [ $gpu = sdl ]
	qa -vga qxl
	qa -display sdl
	qa -device virtio-tablet,wheel-axis=true
else if [ $gpu = spice ]
	qa -vga qxl
	qa -device virtio-serial
	qa -chardev spicevmc,id=vdagent,debug=0,name=vdagent
	qa -device virtserialport,chardev=vdagent,name=com.redhat.spice.0
	#qa -display spice-app
	# The viewer spice-app launches seems to be kinda broken
	# Seamless migration seems very WIP, let's try it :)
	qa -spice unix,addr=$SPICE_SOCKET,disable-ticketing,seamless-migration=on,gl=on
	#trap "rm $SPICE_SOCKET" EXIT
else if [ $gpu = rx580 ]
	if [ (id -u) != 0 ]
		echo "This script must be ran as root"
		exit 1
	end

	# Both the GPU and embedded audio device need to be dealt with
	set gpu_ids "1002 67df" "1002 aaf0"
	set gpu_paths 0000:08:00.0 0000:08:00.1

	# Should not be necessary with vendor-reset module
	#./polaris-unbind-and-reset.sh $gpu_paths[1]

	# See https://arseniyshestakov.com/2016/03/31/how-to-pass-gpu-to-vm-and-back-without-x-restart/
	# TODO: Command sequence vs kernel docs: https://www.kernel.org/doc/Documentation/vfio.txt
	modprobe vfio-pci
	for i in (seq 1 (count $gpu_ids))
		# TODO: Fish echo + redirection seems to be broken!
		# Using tee as a workaround
		echo "- "$gpu_ids[$i]
		echo "  vfio_pci: writing new_id "$gpu_ids[$i]
		echo $gpu_ids[$i] | tee /sys/bus/pci/drivers/vfio-pci/new_id
		echo "  unbinding "$gpu_paths[$i]
		echo $gpu_paths[$i] | tee /sys/bus/pci/devices/$gpu_paths[$i]/driver/unbind
		echo "  vfio_pci: binding "$gpu_paths[$i]
		echo $gpu_paths[$i] | tee /sys/bus/pci/drivers/vfio-pci/bind
		echo "  vfio_pci: writing remove_id "$gpu_ids[$i]
		echo $gpu_ids[$i] | tee /sys/bus/pci/drivers/vfio-pci/remove_id
	end

	qa -device pcie-root-port,bus=pcie.0,addr=1c.0,multifunction=on,port=1,chassis=1,id=root.1
	# Using the ROM file isn't strictly necessary for Windows hosts with
	# drivers, but the UEFI framebuffer (and maybe driverless operation)
	# doesn't work without it
	qa -device vfio-pci,host=$gpu_paths[1],bus=root.1,addr=00.0,multifunction=on,romfile=rx580.rom
	qa -device vfio-pci,host=$gpu_paths[2],bus=root.1,addr=00.1

	# Mouse and keyboard (TODO: switchable?)
	qa -device usb-host,hostbus=3,hostport=1
	qa -device usb-host,hostbus=3,hostport=2

	qa -vga none
	qa -display none

	# Alsa in QEMU doesn't seem to work for anyone...
	#qa -audiodev id=alsa,driver=alsa,out.dev=sysdefault:CARD=Generic
	#qa -device hda-output,audiodev=alsa
	# TODO: Parametrise
	cp /home/dvdkon/.config/pulse/cookie ~/.config/pulse/cookie
	qa -audiodev id=pa,driver=pa,server=/run/user/1000/pulse/native,out.mixing-engine=off
	qa -device ich9-intel-hda
	qa -device hda-output,audiodev=pa
	#qa -device usb-audio,audiodev=pa,multi=on
else
	qa -display none
end

qa -nic bridge,br=br0,model=virtio-net-pci

if pgrep smbd >/dev/null
	qa -nic user,model=virtio-net-pci,restrict=on,guestfwd=tcp:10.0.2.100:445-tcp:127.0.0.1:20445,hostfwd=tcp::23389-:3389
else
	qa -nic user,model=virtio-net-pci,restrict=on,hostfwd=tcp::23389-:3389
end

qa -drive file=main_disk.qcow2,if=virtio,media=disk,aio=native,cache=none,index=0,discard=unmap,detect-zeroes=unmap
qa -drive file=/mnt/datahdd/win_data.qcow2,if=virtio,media=disk,aio=native,cache=none,index=1,discard=unmap,detect-zeroes=unmap

qa $argv[4..-1]

if [ $USER != root ]
	sudo true # Just to get the password
	sudo qemu-system-x86_64 $qemu_args &
	sleep 1
	[ -e $SPICE_SOCKET ]; and sudo chown $USER $SPICE_SOCKET
else
	qemu-system-x86_64 $qemu_args &
	set qemu_pid (jobs -lp)
	# TODO: Also do this for the non-root variant
	echo -100 > /proc/$qemu_pid/oom_score_adj
	sleep 1
end

if [ $gpu = spice ]
	set spice_url spice+unix://$SPICE_SOCKET
	#env GDK_SCALE=1 GDK_DPI_SCALE=1 remote-viewer $spice_url
	env GDK_SCALE=1 GDK_DPI_SCALE=1 spicy --uri=$spice_url
else if [ $gpu = rdp ]
	sleep 30
	xfreerdp /v:127.0.0.1:23389 /dynamic-resolution /u:$USER
end
