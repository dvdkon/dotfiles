GPU PASSTHROUGH
===============

GPU passthrough on zen2itx with a RX580 requires a kernel patch (just the
patch, not the script, weirdly enough, but the script might prevent crashes?):
https://gist.github.com/shatsky/2c8959eb3b9d2528ee8a7b9f58467aa0

COMMON ERRORS
=============

"No new devices found"
----------------------

This looks like a driver problem, but it may not be the case. If you get the
driver selection screen before Windows asks you for a product key / edition, it
means that the installer wasn't able to find the install disk. This is probably
caused by using a DVD image as a disk or vice versa. For me, loading it into
QEMU with `-drive media=cdrom,index=2,file=...` fixed this problem. You can also
try loading your image as a USB mass storage device, like so
`-drive media=disk,if=none,id=install,format=raw,file=... -device usb-storage,drive=install`

POST-INSTALL
============

Removing Appx packages
----------------------

A lot of the built-in apps can be removed via powershell::

	Get-AppXPackage | Remove-AppXPackage
	Get-AppXProvisionedPackage -Online | Remove-AppXProvisionedPackage -Online


Disabling hibernation and Fast Startup
--------------------------------------

This saves disk space and might make the VM more resilient::

	powercfg -hibernate off

Removing Windows "features"
---------------------------

Some unneeded features can be removed via "Manage optional features" and "Turn
Windows features on or off"

Some have to be removed another way, like so::

	Get-WindowsCapability -Online -Name *speech* | ?{$_.State -eq "Installed"} | Remove-WindowsCapability -Online

HOW TO INSTALL FROM ESD
=======================

The easiest way is to get a normal Windows install image, modify the
install.wim and then attach it to QEMU as a USB disk. The .esd contains all the
files necessary to create a bootable image, but I'm too lazy to write a script
for that.

Wimlib commands example::

	set dest /mnt/tmp1
	set src ~/Downloads/something.esd
	rm $dest/sources/boot.wim
	wimexport $src 2 $dest/sources/boot.wim --compress=LZX
	wimexport $src 3 $dest/sources/boot.wim --compress=LZX
	# You might need to replace .wim with .esd
	rm $dest/sources/install.wim
	# The number here depends on which edition you want
	wimexport $src 7 $dest/sources/boot.wim --compress=LZX
