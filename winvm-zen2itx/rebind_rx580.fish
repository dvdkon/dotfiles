#!/usr/bin/env fish
if [ (id -u) != 0 ]
	echo "This script must be ran as root"
	exit 1
end

set gpu_path 0000:08:00.0
set sound_path 0000:08:00.1

echo $gpu_path > /sys/bus/pci/devices/$gpu_path/driver/unbind
echo $gpu_path > /sys/bus/pci/drivers/amdgpu/bind
echo $sound_path > /sys/bus/pci/devices/$sound_path/driver/unbind
echo $sound_path > /sys/bus/pci/drivers/snd_hda_intel/bind
