source ~/.zshrc-local

# The following lines were added by compinstall

zstyle ':completion:*' completer _complete _ignored
zstyle ':completion:*' format 'Completing: %d'
zstyle ':completion:*' group-name ''
zstyle ':completion:*' list-colors ''
zstyle ':completion:*' list-prompt %SAt %p: Hit TAB for more, or the character to insert%s
zstyle ':completion:*' matcher-list ''
zstyle ':completion:*' verbose true
zstyle :compinstall filename '/home/dvdkon/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall
# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=100000
SAVEHIST=1000000
# End of lines configured by zsh-newuser-install

# Ctrl-W (mostly)
autoload -U select-word-style
select-word-style bash

#Prompt
autoload colors
colors
PROMPT="%{$PROMPT_COLOUR%}<%{$reset_color%}%n %~%{$PROMPT_COLOUR%}>\
%{$reset_color%} "

#Emacs mode... yes...
bindkey -e

#Keyboard - zkbd
autoload zkbd
source ~/.zkbd/$TERM

[[ -n ${key[Backspace]} ]] && bindkey "${key[Backspace]}" backward-delete-char
[[ -n ${key[Insert]} ]] && bindkey "${key[Insert]}" overwrite-mode
[[ -n ${key[Home]} ]] && bindkey "${key[Home]}" beginning-of-line
[[ -n ${key[PageUp]} ]] && bindkey "${key[PageUp]}" up-line-or-history
[[ -n ${key[Delete]} ]] && bindkey "${key[Delete]}" delete-char
[[ -n ${key[End]} ]] && bindkey "${key[End]}" end-of-line
[[ -n ${key[PageDown]} ]] && bindkey "${key[PageDown]}" down-line-or-history
[[ -n ${key[Up]} ]] && bindkey "${key[Up]}" up-line-or-search
[[ -n ${key[Left]} ]] && bindkey "${key[Left]}" backward-char
[[ -n ${key[Down]} ]] && bindkey "${key[Down]}" down-line-or-search
[[ -n ${key[Right]} ]] && bindkey "${key[Right]}" forward-char
[[ -n ${key[Ctrl-Left]} ]] && bindkey "${key[Ctrl-Left]}" backward-word
[[ -n ${key[Ctrl-Right]} ]] && bindkey "${key[Ctrl-Right]}" forward-word

#Aliases
alias ls="ls --color=auto"
alias info="info --vi-keys"
alias nmap="nmap --system-dns"
alias vim=nvim
alias ack="ack --colour --group"

