#!/usr/bin/env sh
focused_term_id=$(i3-msg -t get_tree | jq '.. | (.nodes? // empty)[] | select(.focused == true and .window_properties.class == "Alacritty") | .window')
focused_term=$(xdotool getwindowpid $focused_term_id)
if [ -n "$focused_term" ]; then
	child=$(ps --ppid $focused_term -o pid= | head -n1 | tr -d ' ')
	child_cwd="$(readlink /proc/$child/cwd)"
	cd "$child_cwd"
fi
exec alacritty
