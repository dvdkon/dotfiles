#!/usr/bin/sh
newmacaddr="$(python -c 'import random; print(":".join(["{:02x}".format(random.randint(0, 0xFF)) for i in range(6)]))')"
iface=wlp2s0
systemctl stop connman
ip link set dev "$iface" down
ip link set dev "$iface" address "$newmacaddr"
ip link set dev "$iface" up
systemctl start connman
