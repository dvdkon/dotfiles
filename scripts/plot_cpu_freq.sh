#!/usr/bin/env sh
data="$(mktemp)"
trap "rm $data" EXIT
while :
do
	sed -nEe 's/^cpu MHz\s*: (.*)$/\1/gp' /proc/cpuinfo >> "$data"
	sleep 1
done &
gnuplot -e "plot \"$data\"; pause 1; reread"
