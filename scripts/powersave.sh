#!/usr/bin/env sh
bspc config top_padding 0
killall polybar
connmanctl disable ethernet
connmanctl disable wifi
connmanctl disable bluetooth

