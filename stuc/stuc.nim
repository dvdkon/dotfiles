import httpclient, os, xmlparser, xmltree, json, strutils, terminal, sugar, uri, algorithm

## CONFIG START

let configLocation = getEnv("HOME") & "/.config/syncthing/config.xml"
let apiBaseUrl = "http://127.0.0.1:8384/rest"

## CONFIG END

let configXml = loadXml(configLocation)
let apiKey = configXml.child("gui").child("apikey").innerText
let authHeaders = newHttpHeaders({"X-API-Key": apiKey})
let globalHttpClient = newHttpClient(headers = authHeaders)

proc pathInFolder(path: string): (string, string) =
    assert(path.isAbsolute())

    let folders =
        global_http_client.get(api_base_url & "/config/folders")
        .body
        .parseJson()
    for folder in folders:
        let folder_path = folder["path"].getStr()
        let folder_id = folder["id"].getStr()
        if path == folder_path:
            return (folder_id, "")
        if path.startsWith(folder_path & "/"):
            return (folder_id, path.substr(folder_path.len() + 1))

    stderr.styledWriteLine(fgRed, "Path not in Syncthing folder")
    quit(2)

proc fetchFileInfo(folder: string, pathInFolder: string): JsonNode =
    let resp =
        global_http_client.get(
            api_base_url & "/db/file?folder=" & folder &
            "&file=" & encodeUrl(pathInFolder))
    if resp.code() == HttpCode(200):
        return resp.body.parseJson()
    elif resp.code() == HttpCode(404):
        return nil
    else:
        stderr.styledWriteLine(fgRed, "Error getting file info: ", fgDefault, resp.body)
        quit(4)

proc cmdLs(path: string) =
    let path = absolutePath(path)
    let (folder, pathInFolder) = pathInFolder(path)
    var files =
        if fileExists(path):
            @[pathInFolder]
        elif dirExists(path):
            collect(for i in walkDir(path, true): joinPath(pathInFolder, i.path))
        else:
            stderr.styledWriteLine(fgRed, "Path does not exist")
            quit(3)
    files.sort()
    for file in files:
        let info = fetchFileInfo(folder, file)
        if info == nil:
            echo("  ", lastPathPart(file))
        else:
            if info["local"]["ignored"].getBool():
                stdout.styledWrite(fgYellow, "✘")
            else:
                stdout.styledWrite(fgGreen, "✔")
            stdout.writeLine(" ", lastPathPart(file))

proc cmdRefresh(path: string) =
    let path = absolutePath(path)
    discard 0

proc print_usage() =
    stderr.writeLine("""
        Usage:
            stuc ls <path>...
            stuc refresh <path>...
    """)
    quit(1)


proc main() =
    if paramCount() < 1:
        printUsage()
    if paramStr(1) == "ls":
        if paramCount() < 2:
            printUsage()
        cmdLs(paramStr(2))
    if paramStr(1) == "refresh":
        if paramCount() < 2:
            printUsage()
        cmdRefresh(paramStr(2))

main()
