import os
import osproc
import system
import strutils
import strformat
import sequtils

let iio_dev_path = "/sys/bus/iio/devices"
let poll_delay = 100
let touch_device = "NTRG0001:01 1B96:1B05"
let wacom_devices = [
    "NTRG0001:01 1B96:1B05 stylus",
    "NTRG0001:01 1B96:1B05 eraser"
]

let orientation_conds = [
    proc(x: float, y: float, z: float): bool =
        y < 5 and x > -3 and x < 3 and z > -5,
    proc(x: float, y: float, z: float): bool =
        y > 5 and x > -3 and x < 3 and z > -5,
    proc(x: float, y: float, z: float): bool =
        y > -3 and y < 3 and x > 3 and z > -5,
    proc(x: float, y: float, z: float): bool =
        y > -3 and y < 3 and x < 3 and z > -5,
]
let orientation_xrandr = ["normal", "inverted", "left", "right"]
let orientation_xinput = [
    "1 0 0 0 1 0 0 0 1",
    "-1 0 1 0 -1 1 0 0 1",
    "0 -1 1 1 0 0 0 0 1",
    "0 1 0 -1 0 1 0 0 1",
]
let orientation_wacom = ["none", "half", "ccw", "cw"]

proc read_float(path: string): float =
    system.readFile(path).strip.parse_float

proc read_int(path: string): int =
    system.readFile(path).strip.parse_int

iterator find_accelerometers(): string =
    for iiodev in os.walk_dirs(iio_dev_path / "*"):
        if exists_file(iiodev / "in_accel_scale"):
            yield iiodev

proc set_orientation(orientation: int) =
    let xrandr_orientation = orientation_xrandr[orientation]
    discard osproc.execProcess(fmt"xrandr --orientation {xrandr_orientation}")

    let xinput_orientation = orientation_xinput[orientation]
    discard osproc.execProcess(
        fmt"xinput set-prop '{touch_device}' " &
        fmt"'Coordinate Transformation Matrix' {xinput_orientation}")

    let wacom_orientation = orientation_wacom[orientation]
    for wacom_device in wacom_devices:
        discard osproc.execProcess(
            fmt"xsetwacom set '{wacom_device}' Rotate {wacom_orientation}")

let accs = to_seq(find_accelerometers())
if accs.len != 1:
    echo("Found ", accs.len, "accelerometers, need exactly 1")
    quit(1)
let acc = accs[0]
let scale = read_float(acc / "in_accel_scale")
let rad2deg = 57.29577951307855

var last_orientation = -1

set_orientation(0)

add_quit_proc proc() {.noconv.} =
    set_orientation(0)

while true:
    os.sleep(poll_delay)

    let x = read_float(acc / "in_accel_x_raw") * scale
    let y = read_float(acc / "in_accel_y_raw") * scale
    let z = read_float(acc / "in_accel_z_raw") * scale

    var orientation: int = -1
    for i, oc in orientation_conds.pairs:
        if oc(x, y, z):
            orientation = i

    if orientation == -1 or last_orientation == orientation:
        continue
    echo(fmt"X={format_float(x, precision=4)} Y={format_float(y, precision=4)} Z={format_float(z, precision=4)}")
    echo("Orientation changed from ", last_orientation, " to ", orientation)
    last_orientation = orientation

    set_orientation(orientation)

