#!/usr/bin/env fish
cd (dirname (status --current-filename))
source ../fishtempl.fish

set WM sway
template_file config.ftp > ../sway/.config/sway/config

set WM i3
template_file config.ftp > ../i3/.config/i3/config
