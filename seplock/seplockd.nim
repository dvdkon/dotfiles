import strformat, strutils, os, posix, osproc, options, net, strtabs, pam_bindings

const desktop_vt_num = 1
const lock_vt_num = 8

var username: string
var uid: Uid
var prompt_process: Option[Process]

proc control_sock_path(): string = fmt"/run/user/{uid}/seplock/seplockd.sock"
proc xdg_runtime_dir(): string = fmt"/run/user/{uid}/seplock"
proc swaysock(): string = fmt"/run/user/{uid}/seplock/sway-ipc.sock"
proc sway_config_path(): string = fmt"/run/user/{uid}/seplock/sway.config"
let prompt_cmd_path = get_app_dir() & "/seplockd_prompt"

proc run_cmd(
        name: string,
        args: openArray[string],
        env: StringTableRef = new_string_table()) =
    let process = start_process(name, args=args, env=env, options={poUsePath})
    let exit_code = process.wait_for_exit()
    if exit_code != 0:
        echo "Command ", name, " exited with ", exit_code

proc prompt_running(): bool =
    if prompt_process.is_none:
        return false
    prompt_process.get().running()

proc start_prompt() =
    if prompt_running():
        echo "Already locked!"
        return

    prompt_process = some(start_process(
        "systemd-run",
        args=["--wait",
         "--setenv", "SWAYSOCK=" & swaysock(),
         "--setenv", "XDG_RUNTIME_DIR=" & xdg_runtime_dir(),
         "--property", "PAMName=login",
         "--property", fmt"User={uid}",
         "--property", "StandardInput=tty",
         "--property", fmt"TTYPath=/dev/tty{lock_vt_num}",
         "sway", "--config", sway_config_path()],
        options={poUsePath}))

proc close_prompt() =
    if not prompt_running():
        echo "Can't close prompt, since it's not running!"
        return

    run_cmd("chvt", [$desktop_vt_num])

    run_cmd("swaymsg", ["exit"],
        new_string_table({"SWAYSOCK": swaysock()}))

# From https://github.com/fowlmouth/nimlibs/blob/master/fowltek/pointer_arithm.nim
# Isn't something like this in the standard library?
proc offset[A](some: ptr A; b: int): ptr A =
    cast[ptr A](cast[int](some) + (b * sizeof(A)))

var global_password: string # appdata_ptr is always null for me?
proc conv_callback(
        num_msg: cint,
        msg: ptr ptr pam_message,
        resp: ptr ptr pam_response,
        appdata_ptr: pointer): cint =
    if num_msg != 1 or msg.offset(0).msg != "Password: ":
        echo "Not sure what PAM wants, failing..."
        return PAM_CONV_ERR
    resp[] = cast[ptr pam_response](malloc(uint(num_msg * sizeof(pam_response))))
    resp.offset(0).resp_retcode = 0
    resp.offset(0).resp = strdup(cstring(global_password))
    PAM_SUCCESS

proc authenticate(password: string): bool =
    var res: int
    var handle: ptr pam_handle_t
    var conv = pam_conv(
        conv: conv_callback,
        appdata_ptr: nil,
    )
    res = pam_start(cstring("login"), cstring(username), addr conv, addr handle)
    assert res == PAM_SUCCESS

    global_password = password
    let auth_res = pam_authenticate(handle, 0)
    global_password = ""

    res = pam_end(handle, auth_res)
    assert res == PAM_SUCCESS

    if auth_res == PAM_SUCCESS:
        true
    else:
        echo "PAM authentication failed with " & $auth_res
        false

proc create_run_dir() =
    let path = xdg_runtime_dir()
    create_dir(path)
    let res = chown(cstring(path), uid, 0)
    assert res == 0

proc create_sway_config() =
    # XXX: Escaping issues shouldn't happen with our choice of paths, but keep
    # an eye out
    write_file(sway_config_path(),
        """
        exec swayidle timeout 10 'swaymsg "output * dpms off"' resume 'swaymsg "output * dpms on"'

        exec squeekboard
        exec gsettings set org.gnome.desktop.a11y.applications screen-keyboard-enabled true

        default_border none

        input type:keyboard {
            # Disable VT switching with CTRL+ALT+F*
            xkb_options srvrkeys:none
        }

        """ &
        fmt"exec ""{prompt_cmd_path}"" ""{control_sock_path()}""")

proc create_control_socket(): Socket =
    var res: cint
    let path = control_sock_path()

    var control_sock_stat: Stat
    res = stat(cstring(path), control_sock_stat)
    if res == 0: # File exists
        remove_file(path)

    let sock = new_socket(Domain.AF_UNIX, SockType.SOCK_STREAM, Protocol.IPPROTO_IP)
    sock.bind_unix(path)

    res = chown(cstring(path), uid, 0)
    assert res == 0

    sock

proc process_commands() =
    let sock = create_control_socket()
    sock.listen()

    while true:
        var client_sock: Socket
        sock.accept(client_sock)

        var cmdline: string
        client_sock.read_line(cmdline)
        strip_line_end(cmdline)
        if cmdline == "lock":
            echo "Locking!"
            start_prompt()
        elif cmdline.len >= 7 and cmdline[0..6] == "unlock ":
            let pass = cmdline[7..^1]
            if authenticate(pass):
                echo "Correct password, unlocking..."
                client_sock.send("correct\n")
                close_prompt()
            else:
                client_sock.send("incorrect\n")
            echo "Result written!"

        client_sock.close()

username = param_str(1)
let passwd = getpwnam(cstring(username))
if passwd == nil:
    echo "Could not resolve user"
    quit 1
uid = passwd.pw_uid

create_run_dir()
create_sway_config()
process_commands()
