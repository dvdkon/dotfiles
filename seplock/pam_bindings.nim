 {.deadCodeElim: on.}
const
  lib* = "libpam.so"
  PAM_SUCCESS* = 0
  PAM_OPEN_ERR* = 1
  PAM_SYMBOL_ERR* = 2
  PAM_SERVICE_ERR* = 3
  PAM_SYSTEM_ERR* = 4
  PAM_BUF_ERR* = 5
  PAM_PERM_DENIED* = 6
  PAM_AUTH_ERR* = 7
  PAM_CRED_INSUFFICIENT* = 8
  PAM_AUTHINFO_UNAVAIL* = 9
  PAM_USER_UNKNOWN* = 10
  PAM_MAXTRIES* = 11
  PAM_NEW_AUTHTOK_REQD* = 12
  PAM_ACCT_EXPIRED* = 13
  PAM_SESSION_ERR* = 14
  PAM_CRED_UNAVAIL* = 15
  PAM_CRED_EXPIRED* = 16
  PAM_CRED_ERR* = 17
  PAM_NO_MODULE_DATA* = 18
  PAM_CONV_ERR* = 19
  PAM_AUTHTOK_ERR* = 20
  PAM_AUTHTOK_RECOVERY_ERR* = 21
  PAM_AUTHTOK_LOCK_BUSY* = 22
  PAM_AUTHTOK_DISABLE_AGING* = 23
  PAM_TRY_AGAIN* = 24
  PAM_IGNORE* = 25
  PAM_ABORT* = 26
  PAM_AUTHTOK_EXPIRED* = 27
  PAM_MODULE_UNKNOWN* = 28
  PAM_BAD_ITEM* = 29
  PAM_CONV_AGAIN* = 30
  PAM_INCOMPLETE* = 31
  PAM_SERVICE* = 1
  PAM_USER* = 2
  PAM_TTY* = 3
  PAM_RHOST* = 4
  PAM_CONV* = 5
  PAM_AUTHTOK* = 6
  PAM_OLDAUTHTOK* = 7
  PAM_RUSER* = 8
  PAM_USER_PROMPT* = 9
  PAM_FAIL_DELAY* = 10
  PAM_XDISPLAY* = 11
  PAM_XAUTHDATA* = 12
  PAM_AUTHTOK_TYPE* = 13
  PAM_SILENT* = 0x8000
  PAM_DISALLOW_NULL_AUTHTOK* = 0x0001

type
  pam_message* {.bycopy.} = object
    msg_style*: cint
    msg*: cstring

  pam_response* {.bycopy.} = object
    resp*: cstring
    resp_retcode*: cint

  pam_conv* {.bycopy.} = object
    conv*: proc (num_msg: cint; msg: ptr ptr pam_message; resp: ptr ptr pam_response;
               appdata_ptr: pointer): cint
    appdata_ptr*: pointer

  pam_handle_t* {.bycopy.} = object


proc pam_start*(service_name: cstring; user: cstring; pam_conversation: ptr pam_conv;
               pamh: ptr ptr pam_handle_t): cint {.importc: "pam_start", dynlib: lib.}
proc pam_set_item*(pamh: ptr pam_handle_t; item_type: cint; item: pointer): cint {.
    importc: "pam_set_item", dynlib: lib.}
proc pam_authenticate*(pamh: ptr pam_handle_t; flags: cint): cint {.
    importc: "pam_authenticate", dynlib: lib.}
proc pam_end*(pamh: ptr pam_handle_t; pam_status: cint): cint {.importc: "pam_end",
    dynlib: lib.}

# This should really be in Nim's standard library
proc malloc*(size: uint): pointer {.importc: "malloc".}
proc strdup*(s: cstring): cstring {.importc: "strdup", header: "<string.h>".}
