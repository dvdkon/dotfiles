import httpclient, os, xmlparser, xmltree, osproc, strutils, sequtils, sugar, json

## CONFIG START

let nm_conn_whitelist_path =
    get_env("HOME") & "/.config/ssyncd/nm_connection_whitelist"
let check_interval_ms = 10000
let config_location = get_env("HOME") & "/.config/syncthing/config.xml"
let api_base_url = "http://127.0.0.1:8384/rest"

## CONFIG END

let nm_connection_whitelist = to_seq(lines(nm_conn_whitelist_path))

let config_xml = load_xml(config_location)
let api_key = config_xml.child("gui").child("apikey").innerText
let auth_headers = new_http_headers({"X-API-Key": api_key})
let global_http_client = new_http_client(headers = auth_headers)

proc pause_all() =
    discard global_http_client.post(api_base_url & "/system/pause")

proc resume_all() =
    # Resume folders
    let folders =
        global_http_client.get(api_base_url & "/config/folders")
        .body
        .parse_json()
    for folder in folders:
        let folder_id = folder["id"].get_str()
        discard global_http_client.patch(
            api_base_url & "/config/folders/" & folder_id,
            "{\"paused\": false}")

    # Resume all peers
    discard global_http_client.post(api_base_url & "/system/resume")

proc nm_get_active_conns(): seq[string] =
    let (out_str, _) = execCmdEx("nmcli -g uuid connection show --active")
    out_str.split_lines().filter(l => not l.is_empty_or_whitespace())

proc check_conn_whitelist() =
    let conns = nm_get_active_conns()
    if conns.any(c => nm_connection_whitelist.contains(c)):
        resume_all()
    else:
        pause_all()

proc main() =
    while true:
        sleep(check_interval_ms)
        try:
            check_conn_whitelist()
        except:
            echo("Periodic step failed with ", get_current_exception_msg())

main()
