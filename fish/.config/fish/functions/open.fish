function open
	if [ (count $argv) -eq 1 ]
		if [ ! -e $argv[1] ]
			echo "File does not exist!" >&2
			return 1
		end
		xdg-open $argv[1] &> /dev/null &
	else
		$argv &> /dev/null &
	end
	disown
end

