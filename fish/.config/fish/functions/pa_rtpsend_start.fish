function pa_rtpsend_start
	if [ (count $args) -ne 2 ]
		echo "Usage: <dest IP> <port>"
		return
	end

	pactl load-module module-rtp-send source=default.monitor destination_ip=$args[1] port=$args[2]
end

