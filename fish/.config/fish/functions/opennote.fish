function opennote
	cd $notes_dir

	set note_file (find -name "*.note" \
		| sed -e 's|^./||g; s|\.note$||g' \
		| fzf --preview="cat {}").note
	[ -z $note_file ]; or nvim $note_file

	cd -
end

