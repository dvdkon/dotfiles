function ufmt \
		-d "Mount FUSE filesystem as user as per ~/.config/ufmtab" \
		-a mountdir

	set default_opts "reconnect,ServerAliveInterval=15,ServerAliveCountMax=5"

	set mountdir (readlink -f $mountdir)
	while read line
		set dir (echo $line | tr -s \t | cut -d \t -f 2 | sed -e "s|~|$HOME|g")
		set dir (readlink -f $dir)
		if [ $dir = $mountdir ]
			set addr (echo $line | tr -s \t | cut -d \t -f 1)
			set opts (echo $line | tr -s \t | cut -d \t -f 3)
			if [ -z $opts ]
				set opts $default_opts
			else
				set opts $default_opts,$opts
			end
			sshfs -o $opts $addr $dir
			return
		end
	end < ~/.config/ufmtab
	echo "No matching mount found" 1>&2
	return 1
end
