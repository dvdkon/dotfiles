function openbookmark
	pushd $notes_dir

	set bookmark_file (find -name "*.bookmark" \
		| sed -e 's|^./||g; s|\.bookmark$||g' \
		| fzf --preview="cat {}").bookmark
	[ -z $bookmark_file ]; or xdg-open (cat $bookmark_file)

	popd
end

