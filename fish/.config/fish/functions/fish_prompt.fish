function fish_prompt
	set_color $prompt_color
	echo -n "<"
	if [ -n "$IN_NIX_SHELL" ]
		set_color $prompt_color --dim
		echo -n "NS "
	end
	set_color normal
	echo -n "$USER "(prompt_pwd)
	set_color $prompt_color
	echo -n "> "
end
