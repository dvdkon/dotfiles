function decompile_apk
	if [ (count $argv) -eq 0 ]
		echo "Usage: decompile_apk <apk file> [<output dir>] [<decompiler>] [<decompiler arg...>]"
		return
	end
	if [ (count $argv) -ge 3 ]
		set decompiler $argv[3]
	else
		set decompiler cfr
	end
	if [ (count $argv) -eq 1 ]
		set outdir (echo $argv[1] | sed -Ee 's/\.apk$//g')_$decompiler
	else
		set outdir $argv[2]
	end
	mkdir -p $outdir
	if [ $decompiler = jadx ]
		jadx -d $outdir $argv[1] $argv[4..-1]
		return
	end

	set jarpath (mktemp -u).jar
	if which d2j-dex2jar &>/dev/null
		d2j-dex2jar $argv[1] -o $jarpath
	else
		dex2jar $argv[1] -o $jarpath
	end
	if [ $decompiler = procyon ]
		if which procon-decompiler
			procyon-decompiler -o $outdir $jarpath $argv[4..-1]
		else
			procyon -o $outdir $jarpath $argv[4..-1]
		end
	else if [ $decompiler = cfr ]
		cfr --outputdir $outdir $jarpath $argv[4..-1]
	else
		echo "Unknown decompiler"
	end
	rm $jarpath
end

