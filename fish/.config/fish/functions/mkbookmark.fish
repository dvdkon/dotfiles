function mkbookmark -a name url
	if [ -z $url ]
		set url $name
		set name 
	end
	if [ -d $name ]
		set url_for_name (echo $url | sed -e 's|^[a-z]\+://||; s|/$||g; s|/|_|g')
		set name $name/$url_for_name
	end
	if echo $url | grep -qv "^[a-z]\+://"
		echo "Adding http://"
		set url http://$url
	end
	echo $url > $name.bookmark
end

