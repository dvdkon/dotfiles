umask 002

alias info "info --vi-keys"
alias nmap "nmap --system-dns"
alias vim "nvim"
alias ack "ack --colour --group"
alias ag "ag --color --group"
alias mpv "mpv --no-audio-display"
alias feh "feh --scale-down"
alias xfreerdp "xfreerdp /client-hostname:tsclient /u:user"
alias cal "cal --monday"
alias 7z "7z -mhe=on"
alias pandoc "pandoc -s --pdf-engine lualatex -V papersize:a4 -V csquotes"
alias bomk mkbookmark
alias boop openbookmark
alias noop opennote

if [ -z $FISH_INITIALISED ]
	set -x FISH_INITIALISED 1

	set -gx PATH ~/.bin ~/.local/bin ~/.local/go/bin ~/.local/npm_pkgs/bin ~/.dotnet/tools ~/.gem/ruby/*/bin ~/.cargo/bin ~/.nimble/bin ~/.local/share/coursier/bin $PATH
	set -gx EDITOR nvim
	set -gx PAGER less
	set -gx BROWSER firefox
	set -gx SXHKD_SHELL /usr/bin/bash
# I have no idea what makes Go think it can just create a non-hidden
# directory in my ~
	set -gx GOPATH ~/.local/go

	set -x _JAVA_OPTIONS '-Dsun.java2d.opengl=true -Dawt.useSystemAAFontSettings=on -Dswing.aatext=true'

	#set -x DOTNET_ROOT /usr/share/dotnet
	# Shouldn't be necessary
	#set -x MSBuildSDKsPath /usr/share/dotnet/sdk/(dotnet --version)/Sdks
end

set fish_color_command bryellow
set fish_color_param normal
set fish_color_quote brgreen
set fish_color_comment white
set fish_color_error brmagenta

set fish_greeting ""

set notes_dir /mnt/data/projects/hnotes

source ~/.config/fish/config-local.fish

# opam configuration
source /home/dvdkon/.opam/opam-init/init.fish > /dev/null 2> /dev/null; or true
