#!/usr/bin/env fish
set -x GDK_BACKEND wayland
set -x QT_QPA_PLATFORM 'wayland;xcb'
set -x SDL_VIDEODRIVER wayland
set -x MOZ_ENABLE_WAYLAND 1
set -x _JAVA_AWT_WM_NONREPARENTING 1

set -x XDG_CURRENT_DESKTOP sway
# Breaks antialiasing and theming
set -x GTK_USE_PORTAL 1

exec systemd-cat -t sway sway
