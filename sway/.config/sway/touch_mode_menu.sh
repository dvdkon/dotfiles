#!/usr/bin/env sh
choices="$(cat <<EOF
Close window
Toggle border

Open Foot

Restart Squeekboard
Reload input devices
EOF
)"
choice="$(echo "$choices" | bemenu \
	--list 10\
	--center \
	--width-factor 0.3 \
	--line-height 50 \
	--fn 'Liberation Mono [25]' \
	--tf '#71b4f2' \
	--cb '#000000' \
	--cf '#121212' \
	--nb '#000000' \
	--hb '#71b4f2' \
	--hf '#000000' \
	-B 2 \
	--bdr '#71b4f2' \
	--no-spacing \
	--prompt 'Touch mode actions')"

if [ "$choice" = "Close window" ]; then
	swaymsg kill
elif [ "$choice" = "Toggle border" ]; then
	current=$(swaymsg -t get_tree \
		| jq -r '.. | select(.type? == "con" and .focused).border')
	if [ "$current" = pixel ]; then
		swaymsg border normal
	else
		swaymsg border pixel
	fi
elif [ "$choice" = "Open Foot" ]; then
	foot &
	disown -h %1
elif [ "$choice" = "Restart Squeekboard" ]; then
	systemctl --user restart squeekboard.service
elif [ "$choice" = "Reload input devices" ]; then
	for dev in $(swaymsg -t get_inputs --raw | jq -r .[].identifier); do
		swaymsg input $dev events disabled
		swaymsg input $dev events enabled
	done
fi
