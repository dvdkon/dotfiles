#!/usr/bin/env sh
focused_term=$(swaymsg -t get_tree | jq '.. | (.nodes? // empty)[] | select(.focused == true and .app_id == "foot") | .pid')
if [ -n "$focused_term" ]; then
	child=$(ps --ppid $focused_term -o pid= | head -n1 | tr -d ' ')
	child_cwd="$(readlink /proc/$child/cwd)"
	cd "$child_cwd"
fi
exec foot
