#!/usr/bin/env fish
set outputs_json (swaymsg -t get_outputs)
set current (echo $outputs_json | jq -r ".[] | select(.focused == true).name")
set outputs (echo $outputs_json | jq -r .[].name)
set take_next 0
for output in $outputs $outputs
	if [ $take_next -eq 1 ]
		swaymsg focus output $output
		exit
	end
	if [ $output = $current ]
		set take_next 1
	end
end
