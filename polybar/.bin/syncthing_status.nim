import httpclient, os, xmlparser, xmltree, json

let config_location = get_env("HOME") & "/.config/syncthing/config.xml"
let config_xml = load_xml(config_location)
let api_key = config_xml.child("gui").child("apikey").innerText

let api_base_url = "http://127.0.0.1:8384/rest"
let auth_headers = new_http_headers({"X-API-Key": api_key})
let global_http_client = new_http_client(headers = auth_headers)

let folder_id = param_str(1)

while true:
    let resp = global_http_client.get_content(
        api_base_url & "/db/status?folder=" & folder_id)
    let json = resp.parse_json(resp)
    let status = json["state"].get_str()

    let (icon, text) =
        case status
        of "": ("", "Paused")
        of "scanning": ("", "Scanning...")
        of "idle": ("", "Idle")

    sleep(5)
    
