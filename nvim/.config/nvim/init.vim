" Local machine-specific config file
" Variables it should define:
"  g:config_heavyweight = 0/1
runtime init_local.vim

autocmd!
let $NVIM_TUI_ENABLE_TRUE_COLOR = 1

call plug#begin()

" Themes
Plug 'rktjmp/lush.nvim'
Plug 'npxbr/gruvbox.nvim'

if !g:config_system_treesitter
	" Syntax support via treesitter
	Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}
endif
Plug 'nvim-treesitter/playground'

" Syntax for languages not supported by treesitter
Plug 'dag/vim-fish'
Plug 'lepture/vim-jinja'
Plug 'slim-template/vim-slim', {
\	'for': ['slim']
\}
Plug 'huiyiqun/elvish.vim'
Plug 'lifepillar/pgsql.vim'
Plug 'zah/nim.vim'
Plug 'kelwin/vim-smali'
Plug 'isobit/vim-caddyfile'
Plug 'chrisbra/csv.vim'
Plug 'kergoth/vim-bitbake'
Plug 'vmchale/ion-vim'
Plug 'sirtaj/vim-openscad'
Plug 'lumiliet/vim-twig'

" Autocompletion
Plug 'hrsh7th/nvim-cmp'
Plug 'hrsh7th/cmp-buffer'
Plug 'hrsh7th/cmp-path'
Plug 'hrsh7th/cmp-cmdline'
Plug 'L3MON4D3/LuaSnip' " TODO: Am I using this?
Plug 'saadparwaiz1/cmp_luasnip'

" More complex plugins
if g:config_heavyweight
	Plug 'neovim/nvim-lspconfig'
	Plug 'hrsh7th/cmp-nvim-lsp'
	Plug 'mfussenegger/nvim-jdtls'
	Plug 'ionide/Ionide-vim'

	Plug 'dhruvasagar/vim-table-mode'
	Plug 'lervag/vimtex', {
	\	'for': ['tex']
	\}

	Plug 'jmbuhr/otter.nvim'
	Plug 'quarto-dev/quarto-nvim'
	Plug 'benlubas/molten-nvim'
else
	Plug 'PhilT/vim-fsharp' " TODO: Is this needed with Treesitter?
endif

Plug 'djoshea/vim-autoread', {
\	'on': ['WatchForChanges'],
\}

Plug 'junegunn/fzf'
Plug 'junegunn/fzf.vim'

" Automatically set indent settings
Plug 'editorconfig/editorconfig-vim'

" Git integration
Plug 'airblade/vim-gitgutter'
Plug 'jreybert/vimagit'

" Keybinding hints
Plug 'folke/which-key.nvim'

call plug#end()

"Leader key
let mapleader=" "
let maplocalleader=" "

nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l

noremap <C-PageUp> :<C-U>tabprev<CR>
noremap <C-PageDown> :<C-U>tabnext<CR>
noremap <C-S-PageUp> :<C-U>tabm -1<CR>
noremap <C-S-PageDown> :<C-U>tabm +1<CR>

nnoremap <M-Left> <C-O>
nnoremap <M-Right> <C-I>

let g:sql_type_default = 'pgsql'
let g:pgsql_pl = ['python']

"Compe autocomplete
set completeopt=menuone,noselect
lua <<EOF
	local cmp = require("cmp")
	cmp.setup {
		snippet = {
			expand = function(args)
				require('luasnip').lsp_expand(args.body)
			end,
		},
		completion = {
			autocomplete = false,
		},
		mapping = {
			["<C-Space>"] = cmp.mapping.complete(),
			["<CR>"] = cmp.mapping.confirm(),
			["<Tab>"] = cmp.mapping.complete_common_string(),
			["<C-e>"] = cmp.mapping.close(),
			["<C-d>"] = cmp.mapping.scroll_docs(4),
			["<C-u>"] = cmp.mapping.scroll_docs(-4),
			["<Up>"] = cmp.mapping.select_prev_item(),
			["<Down>"] = cmp.mapping.select_next_item(),
		},
		sources = {
			{ name = "nvim_lsp" },
			{ name = 'otter' },
			{ name = "luasnip" },
			{ name = "path" },
			{ name = "buffer" },
		},
	}
EOF

" LSP
if g:config_heavyweight
	lua <<EOF
		local lspconfig = require("lspconfig")

		function setup_lsp_keybinds(bufnr)
			local function map(mode, key, cmd)
			    vim.api.nvim_buf_set_keymap(bufnr, mode, key, cmd,
			  	{ noremap=true, silent=true })
			end

			-- See `:help vim.lsp.*` for documentation on any of the below functions
			map('n', '<leader>D', '<cmd>lua vim.lsp.buf.declaration()<CR>')
			map('n', '<leader>d', '<cmd>lua vim.lsp.buf.definition()<CR>')
			map('n', '<leader>h', '<cmd>lua vim.lsp.buf.hover()<CR>')
			map('n', '<leader>i', '<cmd>lua vim.lsp.buf.implementation()<CR>')
			map('n', '<leader>H', '<cmd>lua vim.lsp.buf.signature_help()<CR>')
			map('n', '<leader>wa', '<cmd>lua vim.lsp.buf.add_workspace_folder()<CR>')
			map('n', '<leader>wr', '<cmd>lua vim.lsp.buf.remove_workspace_folder()<CR>')
			map('n', '<leader>wl', '<cmd>lua print(vim.inspect(vim.lsp.buf.list_workspace_folders()))<CR>')
			map('n', '<leader>T', '<cmd>lua vim.lsp.buf.type_definition()<CR>')
			map('n', '<leader>n', '<cmd>lua vim.lsp.buf.rename()<CR>')
			map('n', '<leader>a', '<cmd>lua vim.lsp.buf.code_action()<CR>')
			map('n', '<leader>r', '<cmd>lua vim.lsp.buf.references()<CR>')
			map('n', '<leader>e', '<cmd>lua vim.diagnostic.open_float()<CR>')
			map('n', '<leader>[', '<cmd>lua vim.diagnostic.goto_prev()<CR>')
			map('n', '<leader>]', '<cmd>lua vim.diagnostic.goto_next()<CR>')
			map('n', '<leader>q', '<cmd>lua vim.diagnostic.setloclist()<CR>')
			map("n", "<leader>f", "<cmd>lua vim.lsp.buf.formatting()<CR>")
		end

		local on_attach = function(client, bufnr)
			local function opt(...) vim.api.nvim_buf_set_option(bufnr, ...) end

			opt('omnifunc', 'v:lua.vim.lsp.omnifunc')

			setup_lsp_keybinds(bufnr)
		end

	  local capabilities = require('cmp_nvim_lsp').default_capabilities()

		-- npm install -g pyright
		lspconfig.pyright.setup { on_attach = on_attach, capabilities = capabilities }
		lspconfig.ruff.setup { on_attach = on_attach, capabilities = capabilities }
		-- pip install python-lsp-server[all]
		--lspconfig.pylsp.setup { on_attach = on_attach, capabilities = capabilities }
		--lspconfig.pylyzer.setup { on_attach = on_attach, capabilities = capabilities }
		-- npm install -g vscode-langservers-extracted
		lspconfig.cssls.setup { on_attach = on_attach, capabilities = capabilities }
		lspconfig.html.setup { on_attach = on_attach, capabilities = capabilities }
		lspconfig.jsonls.setup { on_attach = on_attach, capabilities = capabilities }
		-- npm install -g typescript typescript-language-server
		lspconfig.ts_ls.setup { on_attach = on_attach, capabilities = capabilities }
		-- npm install -g svelte-language-server
		lspconfig.svelte.setup { on_attach = on_attach, capabilities = capabilities }
		lspconfig.volar.setup { on_attach = on_attach, capabilities = capabilities }
		-- https://phpactor.readthedocs.io/en/master/usage/standalone.html#installation-global
		lspconfig.phpactor.setup { on_attach = on_attach, capabilities = capabilities }
		-- pacman -S rust-analyzer
		lspconfig.rust_analyzer.setup {
			on_attach = on_attach,
			capabilities = capabilities,
			settings = {
				["rust-analyzer"] = {
					cargo = {
						target = os.getenv("RUST_TARGET"),
					},
				},
			},
		}
		-- omnisharp-roslyn from AUR
		lspconfig.omnisharp.setup {
			on_attach = on_attach,
			capabilities = capabilities,
			cmd = { "/usr/bin/env", "OmniSharp", "--languageserver" , "--hostPID", tostring(vim.fn.getpid()) },
		}
		-- Bundled with clang
		lspconfig.clangd.setup { on_attach = on_attach, capabilities = capabilities }
		-- nimble install nimlangserver
		-- Doesn't work with stock Arch packages due to missing source fles
		lspconfig.nim_langserver.setup { on_attach = on_attach, capabilities = capabilities }
		lspconfig.hls.setup {
			on_attach = on_attach,
			capabilities = capabilities,
			settings = {
				haskell = {
					formattingProvider = "stylish-haskell",
					plugin = { rename = { config = { crossModule = true } } },
				},
			},
		}
		lspconfig.julials.setup { on_attach = on_attach, capabilities = capabilities }
		lspconfig.gopls.setup { on_attach = on_attach, capabilities = capabilities }
		lspconfig.perlpls.setup { on_attach = on_attach, capabilities = capabilities }
		lspconfig.tinymist.setup {
			on_attach = on_attach,
			capabilities = capabilities,
			settings = {
				exportPdf = "onSave",
			},
			single_file_support = true,
		}

		function init_jdtls()
			require("jdtls").start_or_attach({
				cmd = {
					"/usr/bin/env", "jdtls",
					"-Xms1g",
					-- TODO: What even is this?
					"-data", os.getenv("HOME") .. "/.local/jdtls_workspace"
				},

				root_dir = require("jdtls.setup").find_root({
					".git",
					"mvnw",
					"gradlew",
					"build.xml",
					"pom.xml",
					"settings.gradle",
					"settings.gradle.kts",
					"summary.txt", -- Generated by CFR
				}),
				settings = {
					java = {
						format = {
							settings = {
								url = os.getenv("HOME")
								    .. "/.config/nvim/jdtls/style.xml",
							},
						},
					},
				},
				init_options = {
					bundles = {}
				},
			})

			setup_lsp_keybinds(0)
		end

		require("quarto").setup{
			debug = false,
			closePreviewOnExit = true,
			lspFeatures = {
				enabled = true,
				languages = { "python" },
				chunks = "all",
				diagnostics = {
					enabled = true,
					triggers = { "BufWrite" },
				},
				completion = {
					enabled = true,
				},
			},
			keymap = false,
			--keymap = {
			--	hover = "<leader>h",
			--	definition = "<leader>d",
			--},
		}
EOF

	autocmd FileType java lua init_jdtls()
	autocmd FileType quarto lua setup_lsp_keybinds(0)
endif

"which-key.nvim
lua << EOF
  require("which-key").setup { }
EOF

"Git integration
"let g:gitgutter_map_keys = 0
"let g:gitgutter_enabled = 0

nmap <leader>gg <plug>(GitGutterToggle)
nmap <leader>g[ <plug>(GitGutterPrevHunk)
nmap <leader>g] <plug>(GitGutterNextHunk)
nmap <leader>gp <plug>(GitGutterPreviewHunk)
nmap <leader>gs <plug>(GitGutterStageHunk)
nmap <leader>gu <plug>(GitGutterUndoHunk)

"Base vim options
set number
set ruler
set wildmode=longest,list,full
set background=dark

function Set_colorscheme_overrides()
	hi Normal ctermbg=none guibg=none
	hi NonText ctermbg=none guibg=none
	hi SpecialKey ctermbg=none guibg=none gui=italic
	hi LineNr ctermbg=none guibg=none
	hi Comment cterm=italic gui=italic
endfunction
autocmd ColorScheme * call Set_colorscheme_overrides()

set termguicolors " Required by npxbr/gruvbox.nvim
colorscheme gruvbox
let g:gruvbox_contrast_dark = 'hard'

" Convenience shortcuts
" Reload syntax from start of file
nmap <leader>s :<C-u>syn sync fromstart<CR>

"Language config
filetype plugin on

" Defaults
set tabstop=4
set shiftwidth=4
set softtabstop=4
set smarttab
set autoindent
set smartindent
set scrolloff=1
autocmd FileType * setlocal noexpandtab shiftwidth=4 softtabstop=4 tabstop=4

" nvim-treesitter
lua <<EOF
	conf = {
		parser_install_dir = os.getenv("HOME") .. "/.local/share/nvim/tree-sitter",
		highlight = {
			enable = true,
			disable = {
				"php", -- Weird highlighting, synID is 0?
			},
		},
		indent = {
			enable = false -- Kinda broken right now
		},
	}
	if not vim.g.config_system_treesitter then
		conf.ensure_installed = "all"
		conf.ignore_install = { "css" } -- Vue/Svelte grammar doesn't know how to switch CSS<->SCSS yet

		-- NixOS is not happy about compiling C++ with plain clang
		require("nvim-treesitter.install").compilers = { "clang++" }
	end
	require("nvim-treesitter.configs").setup(conf)

	local parser_configs = require "nvim-treesitter.parsers".get_parser_configs()
	parser_configs.fsharp = {
		  install_info = {
			url = "https://github.com/Nsidorenco/tree-sitter-fsharp",
			branch = "develop",
			files = {"src/scanner.cc", "src/parser.c" },
			generate_requires_npm = true,
			requires_generate_from_grammar = true
		},
		filetype = "fsharp",
	}
EOF

"vimscript
let g:vim_indent_cont = 0

"Java
autocmd FileType java setlocal expandtab

"YAML
autocmd FileType yaml setlocal expandtab tabstop=2 softtabstop=2 shiftwidth=2

"F#
autocmd FileType fsharp setlocal expandtab comments^=:///
autocmd FileType fsharp lua setup_lsp_keybinds(0)
let g:fsharp#lsp_codelens = 0

"SQL
autocmd FileType sql setlocal expandtab

"Python
autocmd FileType python setlocal expandtab

"Scala
autocmd FileType scala setlocal expandtab
autocmd FileType sbt setlocal expandtab

"C++
autocmd FileType cpp nnoremap <leader>w :<C-U>ClangdSwitchSourceHeader<CR>
autocmd BufRead,BufNewFile *.cp setlocal filetype=cpp

"Nim
autocmd FileType nim setlocal expandtab

"C
autocmd FileType c setlocal expandtab

"Haskell
autocmd FileType haskell setlocal expandtab
autocmd FileType cabal setlocal expandtab

"Nix
autocmd FileType nix setlocal expandtab tabstop=2 softtabstop=2 shiftwidth=2

"Text wrapping for Markdown and TeX
"smartindent sometimes indents in the middle of a paragraph
autocmd FileType markdown setlocal textwidth=79 nosmartindent
autocmd FileType tex setlocal textwidth=79

"Xonsh as python
autocmd BufRead,BufNewFile *.xsh set ft=python

"Typst extension
autocmd BufRead,BufNewFile *.typ set ft=typst

"CSV
let b:csv_arrange_align = 'l*'

"FZF
function Fzf_files_fd()
	call fzf#vim#files('', {'source': 'fd --type f'})
endfunction
nnoremap <leader>z :<C-u>call Fzf_files_fd()<CR>
nnoremap <C-p> :<C-u>call Fzf_files_fd()<CR>

"Highlight trailing whitespace

highlight TrailingWhitespace guibg=yellow guifg=red
match TrailingWhitespace /\s\+\%#\@<!$/

"Don't spellcheck start of sequence (collisions with abbreviations)
set spellcapcheck=

syntax on

set mouse=
set clipboard^=unnamed
set nojoinspaces "No double spaces with gq and J

" Automatic color switching
lua <<EOF
	function load_color_from_file()
		local file = io.open(os.getenv("XDG_RUNTIME_DIR") .. "/color_mode", "r")
		if file == nil then return end
		local color_mode = file:read("*all")
		color_mode = color_mode:gsub("%s+", "")
		if color_mode == "dark" then
			vim.cmd("set background=dark")
		elseif color_mode == "light" then
			vim.cmd("set background=light")
		else
			print("Could not switch theme, unknown color mode: " .. color_mode)
		end
		vim.fn.Set_colorscheme_overrides()
	end

	load_color_from_file()
EOF
autocmd Signal SIGUSR1 lua load_color_from_file()
