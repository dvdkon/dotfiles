;; package.el
(require 'package)
(setq package-enable-at-startup nil)
(setq package-archives
      '(("melpa" . "https://melpa.org/packages/")
        ("marmalade" . "https://marmalade-repo.org/packages/")
        ("gnu" . "https://elpa.gnu.org/packages/")
        ("melpa-stable" . "http://stable.melpa.org/packages/")))
;;(setq package-load-list '((diminish t)
;;						  (bind-key t)
;;						  (use-package t)))
(package-initialize)

;; use-package
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))
(setq use-package-always-ensure t)

;; Move customize out of the way
(setq custom-file "~/.emacs.d/custom.el")
(load custom-file)

;; GUI appearance
(if (display-graphic-p)
    (progn (set-default-font "Liberation Mono-9")
           (add-to-list 'default-frame-alist '(alpha 95 95))
           (scroll-bar-mode -1)
           (menu-bar-mode -1)
           (tool-bar-mode -1)))

;; Appearance
(defun customize-theme ()
  (if (display-graphic-p)
      (progn (set-background-color "#060606")
             (add-to-list 'default-frame-alist '(aplha 95 95)))
    (set-face-background 'default nil))
  (set-face-attribute 'fringe nil :background "#101010")
  (set-face-attribute 'minibuffer-prompt nil :background nil)
  (set-face-attribute 'font-lock-comment-face nil :background nil)
  (with-eval-after-load "term"
    (set-face-attribute 'term nil :background nil))
  (with-eval-after-load "dired"
    (set-face-attribute 'dired-directory nil :background nil)
    (set-face-attribute 'dired-symlink nil :background nil))
  (with-eval-after-load "linum"
    (set-face-attribute 'linum nil :background "#101010"))
  )

(use-package gruvbox-theme
  :config
  (setq gruvbox-contrast 'hard)
  (load-theme 'gruvbox t)
  (customize-theme)
  )

;;Highlight all matches after search
(setq lazy-highlight-cleanup nil)

;;Line numbers in buffers
(global-linum-mode 1)
;;Column number on modeline
(setq column-number-mode t)

;; Common niceties
(show-paren-mode 1)
(setq-default show-paren-mode-delay 0)
;; Hiding any one of the fringes causes awful flickering
;; TODO: Figure out why
;;(fringe-mode '(nil . 0))
(setq-default scroll-conservatively most-positive-fixnum)

;; Don't cover the minibuffer with errors while I'm editing in it!
(setq-default command-error-function
              (lambda (errdata context caller)
                (unless (active-minibuffer-window)
                  (command-error-default-function errdata context caller))))

;;Persistent minibuffer history
(savehist-mode)

;;Autoinsert closing pairable char
;;(electric-pair-mode)

;;Smaller tabs
(setq-default tab-width 4)
(setq c-basic-offset 4)
;;Spaces instead by default
;;(for some reason, some files ended up with a weird mix)
(setq-default indent-tabs-mode nil)

;;Elisp indent settings
(put 'add-hook 'lisp-indent-function 1)

;;Enable C-PgUp
(put 'scroll-left 'disabled nil)

(setq-default fill-column 80)
;;VC is too slow with sshfs, and I'm too lazy to filter
(setq-default vc-handled-backends '())


;; General packages for any mode


(use-package general
  :config
  (general-evil-setup)
  (put 'general-define-key 'lisp-indent-function 2)
  (put 'general-nmap 'lisp-indent-function 2)

  (general-define-key
      :keymaps 'minibuffer-local-map "ESC" 'keyboard-escape-quit)

  (general-define-key
      :states 'normal :prefix "SPC"
      "d" '(:ignore t :which-key "describe")
      "b" '(:ignore t :which-key "buffer")
      "n" '(:ignore t :which-key "neotree")
      "l" '(:ignore t :which-key "language")
      "p" '(:ignore t :which-key "project")
      )

  (general-define-key
      :states 'normal :prefix "SPC d"
      "f" '(describe-function :which-key "function")
      "v" '(describe-variable :which-key "variable")
      "m" '(describe-variable :which-key "mode")
      "k" '(describe-key :which-key "key")
      "F" '((lambda () (interactive) (describe-function (thing-at-point 'symbol)))
            :which-key "function at point")
      "V" '((lambda () (interactive) (describe-variable (thing-at-point 'symbol)))
            :which-key "variable at point"))

  (general-define-key
      :states 'normal :prefix "SPC b"
      "m" '(buffer-menu :which-key "menu")
      "s" '(ivy-switch-buffer :which-key "switch")
      "r" '(rename-buffer :which-key "rename"))

  (general-define-key
      :states 'normal :prefix "SPC n"
      "t" '(neotree-toggle :which-key "toggle")
      "o" '(neotree-find :which-key "open"))
  )

(use-package which-key
  :config
  (which-key-mode)
  (setq which-key-idle-delay 1)
  )

(use-package evil
  :init
  (setq evil-want-C-u-scroll t)
  :config
  (evil-mode 1)

  (global-unset-key (kbd "C-w"))
  (general-define-key :prefix "C-w"
    "<up>" #'evil-window-up
    "<down>" #'evil-window-down
    "<left>" #'evil-window-left
    "<right>" #'evil-window-right)

  (setq evil-default-state 'normal
        evil-emacs-state-modes nil
        evil-insert-state-modes nil
        evil-motion-state-modes nil)

  ;;EVIL everywhere
  ;;Shouldn't this be the default?
  (add-hook 'after-change-major-mode-hook
    (lambda ()
      (unless (equal (buffer-name) "*Completions*")
        (evil-change-to-initial-state))))
  ;;Quit less with :q
  ;;TODO: Prompt to save
  (defun evil-quit-less ()
    (interactive)
    ;;(unless (buffer-file-name)
    ;;  (save-buffer))
    (if (< 1 (length (window-list)))
        (delete-window)
      (if (< 1 (length (eyebrowse--get 'window-configs)))
          (eyebrowse-close-window-config)
        (if (< 1 (length (frame-list)))
            (delete-frame)
          (save-buffers-kill-emacs)))))
  (evil-define-command evil-quit-less)
  (evil-ex-define-cmd "q[uit]" #'evil-quit-less)
  )

(use-package eyebrowse
  :config
  (eyebrowse-mode t)
  (setq eyebrowse-new-workspace t)
  (eyebrowse-setup-opinionated-keys)
  )

(use-package swiper
  :config
  ;;(ivy-mode 1)
  )

(use-package all-the-icons
  :disabled t
  )

(use-package neotree
  :commands neotree
  :config
  (add-hook 'eyebrowse-post-window-switch-hook #'neo-global--attach)
  
  (setq neo-theme 'ascii)
  
  (evil-make-overriding-map neotree-mode-map 'normal)
  (general-define-key :keymaps 'neotree-mode-map :states 'normal
                      "<return>" 'neotree-enter
                      "c" #'neotree-change-root
                      "n" #'neotree-create-node
                      "R" #'neotree-rename-node
                      "d" #'neotree-delete-node
                      "h" #'neotree-hidden-file-toggle
                      "r" #'neotree-refresh
                      "m" #'neotree-rename-node)
  )


(use-package company
  :config
  (global-company-mode)
  (setq company-idle-delay nil)
  (evil-declare-change-repeat #'company-complete)
  )

(use-package company-quickhelp
  :config
  (company-quickhelp-mode)
  )

(use-package smart-tab
  :config
  (global-smart-tab-mode 1)
  ;;TODO: Fix in IELM (Company mode?)
  ;;(setq smart-tab-disabled-major-modes '(ielm))
  (defun smart-tab-call-completion-function ()
    (company-complete))
  )

(use-package highlight-symbol
  :commands highlight-symbol
  )


;; Language support packages


(use-package scala-mode
  :mode (("\\.scala\\'" . scala-mode)
         ("\\.sbt\\'" . fundamental-mode))
  :config
  (setq scala-indent:step 4)
  )

(use-package ensime
  :ensure t
  :pin melpa-stable
  ;;:mode ("\\.\\(scala\\|sbt\\)\\'" . ensime-mode)
  :config
  (setq ensime-startup-notification nil)
  (setq ensime-startup-snapshot-notification nil)
  (general-define-key :keymaps 'ensime-mode-map :states 'normal
                      :prefix "SPC l"
                      "i" '(ensime-import-type-at-point :which-key "import")
                      "e" '(ensime-print-errors-at-point :which-key "error")
                      "c" '(ensime-sbt-do-compile :which-key "compile")
                      "r" '(ensime-sbt-do-run :which-key "run"))
  (setf (cdr (assoc 'var ensime-sem-high-faces))
        '(:inherit font-lock-variable-name-face))
  (setf (cdr (assoc 'varField ensime-sem-high-faces))
        '(:inherit font-lock-variable-name-face))
  )

(use-package hungry-delete
  :config
  (global-hungry-delete-mode)
  (setq hungry-delete-chars-to-skip " \t\f\v")
  )

(use-package jdee
  :disabled t
  ;;:mode ("\\.java\\'" . jdee-mode)
  :config
  (setq jdee-server-dir "~/.local/share/jdee-server/target")
  )

(use-package eclim
  :config
  (setq eclim-accepted-file-regexps '("\\.java\\'"))
  (add-hook 'java-mode-hook #'eclim-mode)
  (setq eclimd-autostart t)
  ;;(global-eclim-mode)
  (setq eclimd-default-workspace "~/.emacs.d/eclim")
  )

(use-package company-emacs-eclim
  :config
  (add-hook 'eclim-mode-hook #'company-emacs-eclim-setup)
  )

(use-package intero
  :config
  ;;(add-hook 'haskell-mode-hook #'intero-mode)
  )

(use-package origami
  :disabled t
  :config
  (defun origami-xml-parser ()
    (lambda (content)
      (message (origami-get-positions (content "<!--\\|<[^/>]*[^/]>")))
      )
    )
  (add-to-list 'origami-parser-alist '(nxml-mode . origami-xml-parser))
  )

;;nXML
(setq rng-schema-locating-files
      '("~/.emacs.d/schemas/schemas.xml"
        "schemas.xml"
        "/usr/share/emacs/25.1/etc/schema/schemas.xml"))
(add-hook 'nxml-mode-hook (lambda () (require 'sgml-mode)))
(add-hook 'nxml-mode-hook 'hs-minor-mode)
(add-to-list 'hs-special-modes-alist
             '(nxml-mode
               "<!--\\|<[^/>]*[^/]>"
               "-->\\|</[^>]*[^/]>"
               "<!--"
               sgml-skip-tag-forward
               nil))
(setq nxml-slash-auto-complete-flag t)

(use-package fsharp-mode
  :mode (("\\\.fs[iylx]?\\'" . fsharp-mode)
         ("\\.fsproj\\'" . nxml-mode))
  )

(use-package rust-mode
  :mode ("\\.rs\\'" . rust-mode)
  )

(use-package racer
  :init
  (add-hook 'rust-mode-hook #'racer-mode)
  (setq racer-rust-src-path "/usr/src/rust/src")
  )

(use-package elpy
  ;;:mode ("\\.py\\'" . elpy-mode)
  :config
  (elpy-enable)
  (setq elpy-rpc-backend "jedi")
  (define-key yas-minor-mode-map (kbd "<tab>") nil)
  (define-key yas-minor-mode-map (kbd "TAB") nil)
  )

(use-package yasnippet
  :disabled t
  :config
  ;; TODO: This doesn't get triggered. Why?
  (define-key yas-minor-mode-map (kbd "<tab>") nil)
  (define-key yas-minor-mode-map (kbd "TAB") nil)
  )

(use-package nix-mode
  :mode ("\\.nix\\'" . nix-mode)
  :config
  (set (make-local-variable 'indent-line-function) #'self-insert-command)
  (setq tab-width 4)
  )

(use-package auctex
  :init
  (setq ConTeXt-Mark-version "IV")
  :config
  (add-hook 'TeX-mode-hook
    (lambda ()
      (add-to-list 'TeX-view-program-selection '(output-pdf "Okular"))
      (turn-on-auto-fill)))
  (general-define-key
      :keymaps 'TeX-mode-map :states 'normal :prefix "SPC l"
      "v" '(TeX-view :which-key "view")
      ;; TODO: Not only ConTeXt
      "c" '((lambda () (interactive) (TeX-command "ConTeXt" #'TeX-master-file))
            :which-key "compile")
      "a" '(TeX-command-run-all :which-key "comile and view")
      "l" '(TeX-recenter-output-buffer :which-key "compile log"))
  )

(use-package projectile
  :config
  (projectile-global-mode)
  (setq projectile-completion-system 'ivy)
  (general-define-key
      :keymaps 'projectile-mode-map :states 'normal :prefix "SPC p"
      "f" '(projectile-find-file :which-key "find file"))
  )

(use-package web-mode
  :mode ("\\.html\\'" . web-mode)
  :config
  (add-hook 'web-mode-hook
    (lambda () (when (string-match-p "/jinja2/" (buffer-file-name))
                 (web-mode-set-engine "jinja2"))))
  )

;;(add-to-list 'load-path "~/.emacs.d/lisp/")
;;(require 'stylus-mode)

(use-package less-css-mode
  :mode ("\\.less\\'" . less-css-mode)
  )
